from sys import stdin

for line in stdin:
    [number, operator, other_number] = line.split()

    if operator == "+":
        print(int(number) + int(other_number))
    else:
        print(int(number) - int(other_number))

