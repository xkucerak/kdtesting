import sys
sys.path.append('..')


from kdtesting import (

    Command,
    CommandSequence,
    Test,
    RefImplTester,

    stdout_processed_equality,
    stdout_equality,
    str_id,

    NumberA,
    StringA,

    ConstantA,
    TupleA,
    ChoiceA,
    ChoiceEQA,
    ListA
)

def add_command() -> Command:
    return Command([
        NumberA(0, 10),
        ConstantA("+"),
        NumberA(0, 10)
    ], {
        "operator": [1],
        "number": [0, 2]
    })


def sub_command() -> Command:
    return Command([
        NumberA(0, 10),
        ConstantA("-"),
        NumberA(0, 10)
    ], {
        "operator": [1],
        "number": [0, 2]
    })


def test_1() -> Test:
    command_sequence = CommandSequence([
        (1, add_command())
    ], 5, 10)

    return Test("test_1", command_sequence)


def test_2() -> Test:
    command_sequence = CommandSequence([
        (1, sub_command())
    ], 5, 10)
    return Test("test_2", command_sequence)


def test_3() -> Test:
    command_sequence = CommandSequence([
        (1, add_command()),
        (2, sub_command())
    ], 5, 10)
    return Test("test_3", command_sequence)


def create_tester(tested_program: str, referenced_program: str) \
        -> RefImplTester:
    tester = RefImplTester("python3", "python3", 5)

    tester.set_test_args([tested_program])
    tester.set_ref_args([referenced_program])

    tester.add_output_test(stdout_equality())
    return tester


if __name__ == "__main__":
    tester = create_tester("tested.py", "reference.py")

    tester.add_test(test_1())
    tester.add_test(test_2())
    tester.add_test(test_3())

    shrunk_tests = tester.run()

    for shrunk_test, shrinks in shrunk_tests:
        print(shrunk_test.name, shrinks)
        print(shrunk_test.to_str())

    tester.save_shrunk(shrunk_tests)
