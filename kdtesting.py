from typing import List, Callable, Tuple, Optional, TypeVar, Generic, Any, \
    Generator, Dict, Sequence
import random
import subprocess
import os

T = TypeVar('T')


###############################################################################
# MISC
###############################################################################


def is_in_range(value: int, start: int, end: int) -> bool:
    return start <= value < end


def header(string: str, lines: bool = False) -> str:

    padding = 8
    length = len(string)

    line = '=' * (length + padding * 2 + 2) + '\n'
    text = '=' * padding + f' {string} ' + '=' * padding + '\n'
    res = text

    if lines:
        res = line + text + line
    return res

###############################################################################
# GENERATORS
###############################################################################


Generatable = Any
ValueGenerator = Callable[[], Generatable]


########################################
# ATOMS
########################################


def integer_gen(min_value: int, max_value: int) -> Callable[[], int]:
    """

    Returns:
        Callable[[], int]: value generator that returns integer in
                           range [min_value,max_value)
    """
    def generator() -> int:
        return random.randrange(min_value, max_value)
    return generator


def pos_integer_gen(max_value: int) -> Callable[[], int]:
    """
    Returns:
        Callable[[], int]: value generator that returns positive integer
                           (integer in range [1, max_value))
    """
    return integer_gen(1, max_value)


def lower_case_let_gen() -> Callable[[], str]:
    """
    Returns:
        Callable[[], str]: value generator that returns single lower case
                           letter
    """
    def generator() -> str:
        return chr(random.randint(ord('a'), ord('z')))
    return generator


def upper_case_let_gen() -> Callable[[], str]:
    """
    Returns:
        Callable[[], str]: value generator that returns single upper case
            letter
    """
    def generator() -> str:
        return chr(random.randint(ord('A'), ord('Z')))
    return generator


def str_gen(char_generator: Callable[[], str], min_size: int, max_size: int) \
        -> Callable[[], str]:
    """
    Returns:
        Callable[[], str]: value generator that returns string with size in
            range [min_size, max_size)
    """
    char_list_generator = listof_gen(char_generator, min_size, max_size)

    def generator() -> str:
        return "".join(char_list_generator())
    return generator


########################################
# GENERICS
########################################


def constant_gen(const: T) -> Callable[[], T]:
    """
    Returns:
        Callable[[], T]: value generator that returns the value it got from
            the arg.
    """
    def generator() -> T:
        return const
    return generator


def oneof_gen(constants: List[T]) -> Callable[[], T]:
    """
    Returns:
        Callable[[], T]: value generator that returns one of the constants from
            the constants list.
    """
    def generator() -> T:
        return random.choice(constants)
    return generator


def vector_gen(gen: Callable[[], T], size: int) -> Callable[[], List[T]]:
    """
    Returns:
        Callable[[], List[T]]: value generator that returns list of elements
            generated by the input generator with size 'size'
    """
    def generator() -> List[T]:
        res = []
        for i in range(size):
            res.append(gen())
        return res
    return generator


def listof_gen(gen: Callable[[], T], min_size: int,
               max_size: int) -> Callable[[], List[T]]:
    """
    Returns:
        Callable[[], List[T]]: value generator that returns list of elements
            generated by the input generator with size in range [min_size,
            max_size)
    """
    def generator() -> List[T]:
        size = random.randrange(min_size, max_size)
        res = []
        for i in range(size):
            res.append(gen())
        return res
    return generator


def tupleof_gen(generators: List[ValueGenerator]) \
        -> Callable[[], Tuple[Generatable, ...]]:
    """
    Returns:
        Callable[[], Tuple[Generatable, ...]]: value generator that returns
            tuple where each value at index 'i' is generated by the ith
            generator from the generators list.
    """
    def generator() -> Tuple[Generatable, ...]:
        return tuple([x() for x in generators])
    return generator


def eqchoices_gen(choices: List[Callable[[], T]]) \
        -> Callable[[], Tuple[int, T]]:
    """
    Returns:
        Callable[[], T]: value generator that returns value provided by one of
            the generators, the generators have equal chance of being chosen
    """
    def generator() -> Tuple[int, T]:
        index = random.randrange(0, len(choices))
        return (index, choices[index]())
    return generator


def choices_gen(choices: List[Tuple[int, Callable[[], T]]]) \
        -> Callable[[], Tuple[int, T]]:
    """
    Returns:
        Callable[[], Tuple[int, T]]: value generator that returns tuple
            (index, value) where index, is the index (in the original list) of
            the generator and value is the value provided by the generator
    """
    size = sum([x[0] for x in choices])

    def generator() -> Tuple[int, T]:
        throw = random.randrange(0, size) - choices[0][0]
        index = 0
        while throw >= 0:
            index += 1
            throw -= choices[index][0]
        return (index, choices[index][1]())

    return generator


def choice_ignore_index(choice_generator: Callable[[], Tuple[int, T]]) \
        -> Callable[[], T]:
    """
    Returns:
        Callable[[], T]: value generator that uses the choices generator, then
            dumps the index information.
    """
    def generator() -> T:
        return choice_generator()[1]
    return generator


def mon_operation_generator(left: Callable[[], T], right: Callable[[], T],
                            operation: Callable[[T, T], T]) \
        -> Callable[[], T]:
    """
    Returns:
        Callable[[], T]: monoid operation applied on the values generated by
            the generators
    """
    def generator() -> T:
        return operation(left(), right())
    return generator

###############################################################################
# ARBITRARY
###############################################################################


class Monoid(Generic[T]):

    def operation(self, a: T, b: T) -> T:
        return a


class Arbitrary(Monoid[T]):
    """
    Arbitrary is an object capable of generating arbitrary value with type T,
    shrinking value to simpler instance, and converting value into string.

    To define Arbitrary you need to define `shrink`, `to_str` has a definition
    but overriding will be probably desirable. `get` is already defined and
    uses the generator.

    Attributes:
        generator (Callable[[], T]): function that returns value in type T
    """

    def __init__(self, generator: Callable[[], T]) -> None:
        self.generator = generator

    def get(self) -> T:
        """
        Returns:
            T: one value from the generator provided
        """
        return self.generator()

    def shrink(self, value: T) -> Generator[T, None, None]:
        """
        Returns:
            T: returns shrunk value
        """
        assert False, "shrink not implemented"
        pass

    def to_str(self, value: T) -> str:
        """
        Returns:
            str: string representation of the value
        """
        return str(value)

    def example(self) -> str:
        return self.to_str(self.get())


class ConstantA(Arbitrary[T]):
    """
    ConstantA returns the value provided and shrinks to nothing.
    """

    def __init__(self, constant: T):
        super().__init__(constant_gen(constant))

    def shrink(self, value: T) -> Generator[T, None, None]:
        yield from ()


class Unshrinkable(Arbitrary[T]):
    """
    UnshrinkableA returns value generated by the input generator,
    shrinks to nothing.
    """

    def __init__(self, generator: Callable[[], T]) -> None:
        super().__init__(generator)

    def shrink(self, value: T) -> Generator[T, None, None]:
        yield from ()


class OneOfA(Arbitrary[T]):
    """
    OneOfA returns one of the constants, shrinks to nothing.
    """

    def __init__(self, constants: List[T]):
        super().__init__(oneof_gen(constants))

    def shrink(self, value: T) -> Generator[T, None, None]:
        yield from ()


class TupleA(Arbitrary[Tuple[Generatable, ...]]):
    """
    TupleA returns tuple where value at index i represents value generated by
    the ith arbitrary. Shrinks value to simpler tuple, where each value is
    shrunk by its own Arbitrary.
    """

    def __init__(self, arbitraries: Sequence[Arbitrary[Generatable]]):
        super().__init__(tupleof_gen([a.generator for a in arbitraries]))
        self.arbitraries = arbitraries
        self.str_start = '('
        self.str_end = ')'
        self.str_del = ', '

    def combine(self, duos: List[Tuple[Generatable, Arbitrary[Generatable]]],
                buffer: List[Generatable], level: int) \
            -> Generator[Tuple[Generatable, ...], None, None]:

        if(level >= len(duos)):
            yield tuple(buffer)

        for val in duos[level][1].shrink(duos[level][0]):
            buffer[level] = val
            yield from self.combine(duos, buffer, level + 1)

    def shrink(self, value: Tuple[Generatable, ...]) \
            -> Generator[Tuple[Generatable, ...], None, None]:

        for index in range(len(value)):
            tup_list = list(value)
            for new_elem in self.arbitraries[index].shrink(value[index]):
                if new_elem:
                    tup_list[index] = new_elem
                    yield tuple(tup_list)

    def operation(self, a: Tuple[Generatable, ...],
                  b: Tuple[Generatable, ...]) \
            -> Tuple[Generatable, ...]:

        return tuple(list(a) + list(b))

    def to_str(self, val: Tuple[Generatable, ...]) -> str:
        r_vals = [a.to_str(v) for a, v in zip(self.arbitraries, val)]

        return self.str_start + self.str_del.join(r_vals) + self.str_end


def permutated_range(start: int, end: int, step: int = 1) -> List[int]:
    """
    Returns:
        range, with random permutation
    """
    res_range = list(range(start, end, step))
    size = len(res_range)

    for i in range(size):
        left = random.randrange(0, size)
        right = random.randrange(0, size)
        res_range[left], res_range[right] = res_range[right], res_range[left]

    return res_range


class ListA(Arbitrary[List[T]]):

    """
    ListA returns list of size in [min_size, max_size) with elements
    generated by the provided arbitrary, shrinks smaller version of the
    list.
    """

    def __init__(self, arbitrary: Arbitrary[T], min_size: int,
                 max_size: int) -> None:
        super().__init__(listof_gen(arbitrary.generator, min_size, max_size))
        self.arbitrary = arbitrary
        self.deep_shrink = True
        self.str_start = '['
        self.str_del = ', '
        self.str_end = ']'

    def shrink(self, value: List[T]) -> Generator[List[T], None, None]:
        if not value:
            yield from ()

        for i in permutated_range(0, len(value)):
            yield (value[:i] + value[i + 1:])

        if self.deep_shrink:
            for i in permutated_range(0, len(value)):
                for shrunk_value in self.arbitrary.shrink(value[i]):
                    result = value[:i] + [shrunk_value] + value[i + 1:]
                    yield result

    def to_str(self, value: List[T]) -> str:
        body = self.str_del.join([self.arbitrary.to_str(val) for val in value])
        return self.str_start + body + self.str_end

    def operation(self, a: List[T], b: List[T]) -> List[T]:
        return a + b


class _ChoiceA(Arbitrary[Tuple[int, Generatable]]):

    def __init__(self, generator: Callable[[], Tuple[int, Generatable]],
                 arbitraries: Sequence[Arbitrary[Generatable]]):
        super().__init__(generator)
        self.arbitraries = arbitraries

    def shrink(self, value: Tuple[int, T]) \
            -> Generator[Tuple[int, T], None, None]:
        choice_index, choice_val = value

        for v in self.arbitraries[choice_index].shrink(choice_val):
            yield (choice_index, v)

    def to_str(self, value: Tuple[int, T]) -> str:
        return self.arbitraries[value[0]].to_str(value[1])


class ChoiceA(_ChoiceA):
    """
    ChoiceA provides tuple of (chosen_index, value) where chosen_index
    is the index of the arbitrary used, value is a value provided to said
    arbitrary, it shrinks to (chosen_index, shrunk_value)
    """

    def __init__(self, choices: Sequence[Tuple[int, Arbitrary[Generatable]]]):
        super().__init__(choices_gen([(v, a.generator) for v, a in choices]),
                         [a for _, a in choices])


class ChoiceEQA(_ChoiceA):
    """
    ChoiceEQA is the same as ChoiceA, but each arbitrary has equal chance of
    being chosen.
    """

    def __init__(self, arbitraries: Sequence[Arbitrary[T]]):
        super().__init__(eqchoices_gen([a.generator for a in arbitraries]),
                         arbitraries)


class NumberA(Arbitrary[int]):
    """
    NumberA returns random integer in range [min_value, max_value), shrinks
    to half of the value and value moved towards zero
    """

    def __init__(self, min_value: int, max_value: int) -> None:
        super().__init__(integer_gen(min_value, max_value))
        self.min_value = min_value
        self.max_value = max_value

    def shrink(self, value: int) -> Generator[int, None, None]:
        if(value != 0):
            half = value // 2
            if is_in_range(half, self.min_value, self.max_value):
                yield half

            closer_to_zero = abs(value - 1) * (abs(value) // value)
            if is_in_range(closer_to_zero, self.min_value, self.max_value):
                yield closer_to_zero

    def operation(self, a: int, b: int) -> int:
        return a + b


class StringA(ListA[str]):
    """
    StringA returns random string of alpha characters with length in range
    [min_value, max_value)
    """

    def __init__(self, min_value: int, max_value: int) -> None:
        super().__init__(Unshrinkable(
            choice_ignore_index(
                eqchoices_gen([lower_case_let_gen(),
                               upper_case_let_gen()]
                              )
            )),
            min_value, max_value)

    def to_str(self, value: List[str]) -> str:
        return ''.join(value)

###############################################################################
# TESTING
###############################################################################


class Test(Generic[T]):
    """
    Arbitrary and its arbitrary value wrapper, used in testing. Both `shrink`
    and `to_str` use arbitraries functions with the same name on the value
    the test holds.
    """

    def __init__(self, name: str, arbitrary: Arbitrary[T],
                 value: Optional[T] = None) -> None:
        self.name = name
        self.value = value if value is not None else arbitrary.get()
        self.arbitrary = arbitrary

    def shrink(self) -> Generator['Test[T]', None, None]:
        for shrunk_value in self.arbitrary.shrink(self.value):
            yield Test(self.name, self.arbitrary, shrunk_value)

    def copy(self) -> 'Test[T]':
        return Test(self.name, self.arbitrary, self.value)

    def to_str(self) -> str:
        return self.arbitrary.to_str(self.value)


ShrunkTest = Tuple[Test[Generatable], int]


class Tester:
    """
    Tester class responsible for shrinking tests, the `run_test` is not
    implemented, as the class is 'abstract'. Logging is done using priority,
    the baseline is 5. Also used for test saving.
    """

    def __init__(self, verbosity: int = 5, max_shrinks: int = 1000) -> None:
        self.tests: List[Test[Generatable]] = []
        self.verbosity = verbosity
        self.out_folder = './kdout/'
        self.max_shrinks = max_shrinks

    def add_test(self, test: Test[Generatable]) -> None:
        self.tests.append(test)

    def run_test(self, test: Test[Generatable]) -> bool:
        assert False, "run_test not implemented"

    def log(self, priority: int, *args, **kwargs) -> None:
        if priority <= self.verbosity:
            print(*args, **kwargs)

    def find_smallest(self, test: Test[Generatable]) -> Optional[ShrunkTest]:

        self.log(7, "the test being run:", test.to_str(), sep="\n")

        if(self.run_test(test)):
            return None

        shrinks = 0
        smallest = test
        testing = True

        while testing and shrinks < self.max_shrinks:
            testing = False
            if shrinks % 10 == 0:
                self.log(4, f"Shrinks {shrinks}")

            self.log(7, f"Current shrink ({shrinks}):", smallest.to_str(),
                     sep="\n")

            for sub_test in smallest.shrink():
                self.log(6, "Running test")
                self.log(7, "the test being run:", sub_test.to_str(), sep="\n")
                result = self.run_test(sub_test)
                self.log(6, f"Test {'passed' if result else 'failed'}")
                if not result:
                    testing = True
                    smallest = sub_test
                    shrinks += 1
                    break
        self.log(2, f"Test shrunk using {shrinks} shrinks")
        self.log(3, smallest.to_str())
        return (smallest, shrinks)

    def run(self) -> List[ShrunkTest]:
        """
        Returns:
            List[ShrunkTest]: shurnk tests combined with the number of
                shrinks it took to create them.
        """
        failed_tests: List[ShrunkTest] = []
        for test in self.tests:
            self.log(1, f"Test start: {test.name}")
            shrunk_version = self.find_smallest(test)
            if shrunk_version:
                self.log(2, "Test failed")
                failed_tests.append(shrunk_version)
            else:
                self.log(2, "Test passed")

        if failed_tests:
            no_tests = len(self.tests)
            no_pass = no_tests - len(failed_tests)
            self.log(1, '\n', header('SUMMARY', self.verbosity > 3),
                     f"Tests failed: {no_pass}/{no_tests} passed\n", sep='\n')
        else:
            self.log(1, '\n', header('SUMMARY', self.verbosity > 3),
                     "Tests passed\n", sep='\n')

        return failed_tests

    def get_path(self, filename: str, folder: Optional[str] = None) -> str:
        out_folder = folder if folder is not None else self.out_folder
        return os.path.join(out_folder, filename)

    def save_tests(self, folder: Optional[str] = None) -> None:
        """
        Saves all the tests (in the original form) as .tst files using their
        name
        """
        os.makedirs(self.out_folder, exist_ok=True)
        for test in self.tests:
            filename = self.get_path(f"{test.name}.tst", folder=folder)
            with open(filename, "w") as original_f:
                original_f.write(test.to_str())

    def print_shrunk(self, shrunk_tests: List[ShrunkTest]) -> None:
        for (test, shrinks) in shrunk_tests:
            print(test.name,
                  f"[{shrinks} shrink{'' if shrinks == 1 else 's'}]")
            print(test.to_str())

    def save_shrunk(self, shrunk_tests: List[ShrunkTest],
                    folder: Optional[str] = None) -> None:
        """
        Saves the shrunk tests as .tst files using their name
        """
        os.makedirs(self.out_folder, exist_ok=True)
        for (test, _) in shrunk_tests:
            filename = self.get_path(f"{test.name}_red.tst", folder=folder)
            with open(filename, "w") as shrunk_f:
                shrunk_f.write(test.to_str())


###############################################################################
# REFERENCE IMPLEMENTATION TESTING
###############################################################################

def str_id(x: str) -> str:
    return x


ProcessOutput = Any
ProcessOutputTest = Callable[[ProcessOutput, ProcessOutput], bool]


def str_to_bytes(v: str) -> bytes:
    return bytes(v, 'utf-8')


def bytes_to_str(v: bytes) -> str:
    return v.decode("utf-8")


def stdout_processed_equality(test_preprocessor: Callable[[str], str],
                              exp_preprocessor: Callable[[str], str]) \
        -> ProcessOutputTest:
    """
    Returns:
        ProcessOutputTest: test that first converts the bytes to string, then
            runs the prerocessors, the compares for equality.
    """
    def res_tester(test_out: ProcessOutput, exp_out: ProcessOutput) -> bool:
        processed_test_out = test_preprocessor(bytes_to_str(test_out.stdout))
        processed_exp_out = exp_preprocessor(bytes_to_str(exp_out.stdout))
        return processed_test_out == processed_exp_out
    return res_tester


def stdout_equality() -> ProcessOutputTest:
    """
    Returns:
        ProcessOutputTest: test that compares the raw outputs
    """
    def res_tester(test_out: ProcessOutput, exp_out: ProcessOutput) -> bool:
        return test_out.stdout == exp_out.stdout
    return res_tester


class RefImplTester(Tester):
    """
    RefImplTester is instance of the Tester class, which uses reference
    implementation to do the testing. It converts each test to string, then
    supplies the string to the tested program and reference implementation,
    then runs the process output tests.

    Use:
        initialize this class using its constructor, then add your process
        output tests using 'add_output_test'. If your program needs some flags
        add them using set_flags.
    """

    def __init__(self, program_tested: str, program_referenced: str,
                 verbosity: int = 5) -> None:

        super().__init__(verbosity)
        self.program_tested = program_tested
        self.program_referenced = program_referenced
        self.args: List[str] = []
        self.test_args: List[str] = []
        self.ref_args: List[str] = []
        self.output_tests: List[ProcessOutputTest] = []

    def set_args(self, args: List[str]) -> None:
        """
        Appends the arguments to both executables BEFORE specific arguments (
        see test_args and ref_args
        """
        self.args = args

    def set_test_args(self, test_args: List[str]) -> None:
        """
        Appends the arguments to the tested executable AFTER shared
        arguments
        """
        self.test_args = test_args

    def set_ref_args(self, ref_args: List[str]) -> None:
        """
        Appends the arguments to the reference executable AFTER shared
        arguments
        """
        self.ref_args = ref_args

    def run_test(self, test: Test[Generatable]) -> bool:
        test_string = test.to_str()

        test_program = subprocess.run(
            [self.program_tested] + self.args + self.test_args,
            capture_output=True,
            input=str_to_bytes(test_string))

        exp_program = subprocess.run(
            [self.program_referenced] + self.args + self.ref_args,
            capture_output=True,
            input=str_to_bytes(test_string))

        res = True

        if self.verbosity >= 8:
            self.log(8, "TESTED STDOUT\n" + bytes_to_str(test_program.stdout))
            self.log(8, "REFERENCE STDOUT\n" +
                     bytes_to_str(exp_program.stdout))

        for process_test in self.output_tests:
            res = res and process_test(test_program, exp_program)

        return res

    def add_output_test(self, test: ProcessOutputTest) -> None:
        self.output_tests.append(test)


class Command(TupleA):
    """
    The same as TupleA, but it converts the values to string using their
    arbitrary, then separates by space.

    If you want create symbolic names for different indexes, use symbols
    variable. If you give symbols Dict[str, List[int]] dictionary, that points
    from symbol, to list of the indeces, where the symbol resides, you can
    use get_symbol function on the value, to retrieve the occurences of said
    symbol in the tuple. (useful in CommandSequence)
    """

    def __init__(self, arbitraries: Sequence[Arbitrary[Generatable]],
                 symbols: Optional[Dict[str, List[int]]] = None) -> None:
        super().__init__(arbitraries)
        self.symbols = symbols if symbols is not None else {}

    def to_str(self, value: Tuple[Generatable, ...]) -> str:
        res = ""
        for i, (v, arb) in enumerate(zip(value, self.arbitraries)):
            res += arb.to_str(v)
            if i < len(value) - 1:
                res += " "
        return res

    def get_symbol(self, symbol: str, value: Tuple[Generatable, ...]) \
            -> List[Generatable]:
        """
        Returns:
            List[Generatable]: list of values that are considered to be the
                symbol byt the self.symbols dictionary.
        """
        if symbol not in self.symbols:
            return []

        return [value[i] for i in self.symbols[symbol]]


CommandAppend = Callable[['CommandSequence', List[Tuple[int, Generatable]]],
                         str]


class CommandSequence(ListA[Tuple[int, Generatable]]):
    """
    CommandSequence is the same as ListA, but it first converts the elements
    to string using their Arbitrary's `to_str` function, then connects them
    using '\n'.

    If you defined symbol dictionaries on your commands correctly, you can
    retrieve list of all occurences of the symbol in the whole sequence using
    `get_symbol`.
    """

    def __init__(self,
                 commands: List[Tuple[int,
                                      Command]],
                 min_size: int,
                 max_size: int) -> None:
        """
        Args:
            commands (List[Tuple[int, Command]]): list of commands, and their
                weight (chance of being chosen).
            min_size (int): min size of the sequence
            max_size (int): max size of the sequence
        """
        super().__init__(ChoiceA(commands), min_size, max_size)
        self.commands = commands
        self.start_str: CommandAppend = lambda s, x: ""
        self.end_str: CommandAppend = lambda s, x: ""

    def to_str(self, values: List[Tuple[int, Generatable]]) -> str:
        body = ""
        for i, val in values:
            body += self.arbitrary.to_str((i, val)) + "\n"
        return self.start_str(self, values) + body \
            + self.end_str(self, values)

    def get_symbol(self, symbol: str, values: List[Tuple[int, Generatable]]) \
            -> List[Generatable]:
        """
        Returns:
            List[Generatable]: list of the occurences of the symbol in the
            whole command sequence.
        """
        res = []
        for i, val in values:
            res += self.commands[i][1].get_symbol(symbol, val)
        return res


###############################################################################
# PROCESS OUTPUT TESTING
###############################################################################

OutputTest = Callable[[Any], bool]


class OutputTester(Tester):

    """
    Output tester simply runs its output_tests on the result of the process
    """

    def __init__(self, program_tested: str, verbosity: int = 5):
        super().__init__(verbosity)
        self.program_tested = program_tested
        self.args: List[str] = []
        self.output_tests: List[OutputTest] = []

    def add_output_test(self, output_test: OutputTest) -> None:
        self.output_tests.append(output_test)

    def set_args(self, args: List[str]) -> None:
        self.args = args

    def run_test(self, test: Test[Generatable]) -> bool:

        test_string = test.to_str()
        process_args = [self.program_tested] + self.args

        test_output = subprocess.run(
            process_args,
            capture_output=True,
            input=str_to_bytes(test_string)
        )

        res = True

        if self.verbosity >= 8:
            self.log(8, "STDOUT\n" + bytes_to_str(test_output.stdout))
            self.log(8, "STDERR\n" + bytes_to_str(test_output.stderr))

        for process_test in self.output_tests:
            res = res and process_test(test_output)

        return res


_VALGRIND_PROBLEM = 11


def valgrind_no_leaks(output: Any) -> bool:

    return output.returncode != _VALGRIND_PROBLEM


def create_valgrind_tester(program: str,
                           args: List[str] = ['--leak-check=full',
                                              '--track-origins=yes'],
                           max_shrinks: Optional[int] = None) -> OutputTester:
    """
    Creates output tester for valgrind
    """
    valgrind_tester = OutputTester('valgrind')
    valgrind_tester.set_args(
        args + [f'--error-exitcode={_VALGRIND_PROBLEM}', program])
    valgrind_tester.add_output_test(valgrind_no_leaks)
    if max_shrinks is not None:
        valgrind_tester.max_shrinks = max_shrinks

    return valgrind_tester
