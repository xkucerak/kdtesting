import sys

sys.path.insert(0, '../..')

from kdtesting import NumberA, create_valgrind_tester, Test

final_arb = NumberA(0, 10)

valgrind_tester = create_valgrind_tester('./main')
valgrind_tester.verbosity = 3
valgrind_tester.add_test(Test('valgrind_test', final_arb))

reduced_tests = valgrind_tester.run()
valgrind_tester.print_shrunk(reduced_tests)
