import sys
sys.path.append("..")

from kdtesting import Test, RefImplTester, stdout_equality, ListA, \
    vector_gen, oneof_gen, Arbitrary, stdout_processed_equality
from typing import Generator, List
import re

BASE58_ALPHABET = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz"


class Base58Block(Arbitrary[List[str]]):
    def __init__(self):
        # I created a new generator using vector_gen and oneof_gen (check the
        # kdtestin.py for details about value generators
        super().__init__(vector_gen(
            oneof_gen(BASE58_ALPHABET), 6))

    def shrink(self, value: str) -> Generator[str, None, None]:
        # You can shrink to nothing if you can't find a good way to shrink your
        # arbitrary
        yield from ()

    def to_str(self, value: List[str]) -> str:
        # vector_gen will create not str but list of chars (List[str]) so let's
        # join them
        return ''.join(value)


class Base58Command(ListA[List[str]]):

    def __init__(self, min_size: int, max_size: int):
        # I just told the List Arbitrary to use Base58Block to generate list
        # elements
        super().__init__(Base58Block(), min_size, max_size)

    def to_str(self, value: List[List[str]]):
        # I first have apply to_str on each element, then join them using ''
        # If you are defining list of commands, you can use Command and
        # CommandSequence from .py or simply define your own arbitraries, then
        # join different elements using '\n'
        stringified = [self.arbitrary.to_str(val) for val in value]
        return ''.join(stringified)


def ignore_numbers(s: str) -> str:
    return re.sub(r'\d+', r'', s)


if __name__ == "__main__":

    # Capturing the arguments from the system
    # example run would be:
    # python3 base58.py testing reference
    [program_tst, program_exp] = sys.argv[1:]

    # Note that the last argument is verbosity, 10 is log everything
    tester = RefImplTester(program_tst, program_exp, 4)

    # Adding args for decoding
    tester.set_args(['-d'])

    # Adding equality output test (works both for string output and bytes
    # output)
    tester.add_output_test(stdout_equality())

    # This test is commented, because the program doesn't have to result in a
    # decodable string

    # tester.add_output_test(stdout_processed_equality(ignore_numbers,
    #                                                ignore_numbers))

    # Scales the tests
    for i in range(1, 10):
        base58_command = Base58Command(i * 10, i * 10 * 2)
        tester.add_test(Test(f"base58_test_{i}", base58_command))

    # Runs the tests and shrinks them (in this example it would remove blocks
    # from the base58 command
    reduced_tests = tester.run()

    # If some tests failed it will both print and save the shrunk tests
    for test, shrinks in reduced_tests:
        print(test.name, shrinks)
        print(test.to_str())
        tester.save_shrunk(reduced_tests)

    # Save all tests in they starting form
    tester.save_tests()
