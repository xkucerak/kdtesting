import sys
sys.path.append("..")

from kdtesting import Command, CommandSequence, ConstantA, StringA

# This creates command in the form:
# git commit -m <string[5,10)>
git_commit = Command([
    ConstantA("git commit -m"),
    StringA(5, 10)
], {
    "message": [1]
})

# This creates command in the form:
# git pull
git_pull = Command([
    ConstantA("git pull")
])

# This creates sequence with pull being twice as likely as happen as commit
command_sequence = CommandSequence([
    (1, git_commit),
    (2, git_pull)
], 10, 15)

# Let't get random value from the command sequence
random_command_sequence = command_sequence.get()

print(command_sequence.to_str(random_command_sequence))

# Let's get the symbol message from commits
messages = command_sequence.get_symbol("message", random_command_sequence)
messages = [''.join(m) for m in messages]
print(messages)

# Let's shrink it 10 times
for i in range(5):
    shrunk_gen = command_sequence.shrink(random_command_sequence)
    random_command_sequence = next(shrunk_gen)
    print(command_sequence.to_str(random_command_sequence))




