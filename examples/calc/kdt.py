import sys
sys.path.append("../..")

from kdtesting import *
from typing import Generator, List, Set

class CommandSequenceStages(TupleA):

    def to_str(self, values) -> str:
        variable_names: Set[str] = set(self.get_symbol("variable", values))
        var_inits = "\n".join(
            [f"{name} = 0" for name in variable_names]) + "\n"
        return var_inits + self.get_str_body(values)

    def get_str_body(self, values) -> str:
        res = ""
        for sequence, arb in zip(values, self.arbitraries):
            res += arb.to_str(sequence)
        return res

    def get_symbol(self, symbol: str, values):
        symbols = []
        for sequence, arb in zip(values, self.arbitraries):
            symbols += arb.get_symbol(symbol, sequence)
        return symbols


if __name__ == "__main__":

    variable_names = OneOfA(["alpha", "beta", "gamma"])
    variable_names_dont_init = OneOfA(["lambda", "omega", "zeta"])
    operations = OneOfA(["+", "-", "*", "\\"])
    number = NumberA(0, 100)

    assignment_command = Command([
        variable_names,
        ConstantA("="),
        variable_names,
        operations,
        number
    ], {"variable": [0, 2]})

    operation_command = Command([
        variable_names_dont_init,
        operations,
        variable_names
    ], {"variable": [0, 2]})

    num_operation_command = Command([
        number,
        operations,
        number
    ])

    command_sequence = CommandSequenceStages([
        CommandSequence([
            (1, assignment_command)
        ], 5, 6),
        CommandSequence([
            (1, operation_command),
            (1, num_operation_command)
        ], 5, 10),
    ])

    tester = RefImplTester("python3", "python3", 10)
    tester.set_test_args(["test.py"])
    tester.set_ref_args(["ref.py"])
    tester.add_output_test(stdout_equality())

    for i in range(10):
        tester.add_test(Test(f"test_{i}", command_sequence))

    print(command_sequence.example())

    reduced_tests = tester.run()

    print(reduced_tests)

    for test, shrink in reduced_tests:
        print(shrink)
        print(test.to_str())

    tester.save_shrunk(reduced_tests)
